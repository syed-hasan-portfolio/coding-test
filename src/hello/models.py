from django.db import models

class Words(models.Model):
    name = models.CharField(max_length=100, verbose_name="word")
    count = models.IntegerField(verbose_name="word count")
    documents = models.CharField(max_length=100, verbose_name="documents")
    sentences = models.CharField(max_length=10000, verbose_name="sentences")
# Create your models here.
