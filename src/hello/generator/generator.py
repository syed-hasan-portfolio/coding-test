import nltk
from hello.generator import country_list
import os
import time
start = time.time()
data_dir = 'data/'
all_counts = dict()

nltk.download('punkt')


class Generator:

    final_list = []

    def __init__(self):
        countries = {country for code, country in country_list.Country}

        for filename in os.listdir(data_dir):
            content = open(data_dir+filename, "r", encoding="utf8").read()

            sentences = nltk.tokenize.sent_tokenize(content)
            for sentence in sentences:
                words = nltk.tokenize.word_tokenize(sentence)
                freqs = nltk.FreqDist(words)

                for word, count in freqs.items():
                    if word in countries:
                        self.list_builder(word, count, filename, sentence)

        self.final_list = sorted(self.final_list,key = lambda x: x[1], reverse=True)

    def list_builder(self, word, count, doc, sentence):
        if len(self.final_list) == 0:
            self.final_list.append([
                word,
                count,
                doc,
                sentence
            ])
        else:
            for item in self.final_list:

                if word in item:

                    item[1] = item[1] + count
                    item[2] = item[2] if doc in item[2] else item[2] + ' ' + doc
                    item[3] = item[3] if sentence in item[3] else item[3] + ' \n\n' + sentence
                    return 0

            self.final_list.append([
                word,
                count,
                doc,
                sentence
            ])
