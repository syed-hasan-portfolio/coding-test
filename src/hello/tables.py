# hello/tables.py
import django_tables2 as tables
from .models import Words

class WordsTable(tables.Table):
    class Meta:
        model = Words
        template_name = "django_tables2/bootstrap.html"
        fields = ("name", "count", "documents", "sentences")