from django.shortcuts import render

# Create your views here.
# hello/views.py
from django_tables2 import SingleTableView

from .models import Words
from .tables import WordsTable
from .generator.generator import Generator


class WordViewList(SingleTableView):
    # clear all data
    # get data
    Words.objects.all().delete()
    g = Generator()
    list = g.final_list
    for item in list:

        Words.objects.bulk_create([
            Words(
                name=item[0],
                count=int(item[1]),
                documents=item[2],
                sentences=item[3]
            )
        ])

    model = Words
    table_class = WordsTable
    template_name = 'hello/words.html'
